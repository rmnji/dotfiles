#!/bin/sh
# Swaybar modules "ponele"

#date and clock
date_formatted=$(date "+%b %d, %H:%M")

#memory usage
memory_usage=$(free -h | awk '/^Mem:/ {print $3 "/" $2}')

#cpu temp
cpu_temp=$(sensors | awk '/^Package/ {print $4}')

#cpu usage

#disk usage
disk_usage=$(df /dev/sda3 -h | awk '/^\/dev\/sda3/ {print $3 "/" $2}')

echo  $disk_usage \| ﬙ $cpu_temp \|  $memory_usage \| $date_formatted \|
