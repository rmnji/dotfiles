#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#bashrc

set -o vi
set show-mode-in-prompt on
set vi-cmd-mode-string "\e[2 q"
set vi-ins-mode-string "\e[6 q"
shopt -s autocd # cd into directory just by typing the directory name.

export PS1="\[$(tput bold)\]\[\033[38;5;137m\]\w\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;136m\]>\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;136m\]\\$\[$(tput sgr0)\] \[$(tput sgr0)\]"

#aliases
alias ls='ls -h --color=auto --group-directories-first'
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

