" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" Declare the list of plugins.
Plug 'mattn/emmet-vim'
Plug 'AndrewRadev/tagalong.vim'
Plug 'townk/vim-autoclose'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

"Plug 'neovim/nvim-lspconfig'
"Plug 'kabouzeid/nvim-lspinstall'
"Plug 'hrsh7th/nvim-compe'

Plug 'chriskempson/base16-vim'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()
