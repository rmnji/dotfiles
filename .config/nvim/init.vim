source $HOME/.config/nvim/settings.vim
source $HOME/.config/nvim/mappings.vim

" Plugins
source $HOME/.config/nvim/plugin/plugs.vim
source $HOME/.config/nvim/plugin/netrw.vim
source $HOME/.config/nvim/plugin/emmet.vim
source $HOME/.config/nvim/plugin/coc.vim

"Theme
source $HOME/.config/nvim/themes/base16.vim
