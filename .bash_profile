#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

#!/bin/sh
# Profile file. Runs on login. Environmental variables are set here.

# Adds `~/.local/bin` to $PATH
PATH="$PATH:/usr/local/bin:/home/$USER/.local/bin"

# Clean up
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

# Start graphical server on tty1 if not already running.
if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
	[[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx -- vt1 &> /dev/null
fi

